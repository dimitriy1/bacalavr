import { ApiResponseOptions } from '@nestjs/swagger';

export const SuccessResponseDocs: ApiResponseOptions = {
  schema: {
    properties: {
      status: { type: 'string', example: 'success' },
    },
  },
};

export const SuccessResponseWithIdDocs: ApiResponseOptions = {
  schema: {
    properties: {
      status: { type: 'string', example: 'success' },
      id: { type: 'string', example: '1312313131313' },
    },
  },
};
