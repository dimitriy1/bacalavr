import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { SortKind } from '../enums/sort-kind.enum';
import { Prisma } from '@prisma/client';

export class SortKindQueryDto {
  @IsOptional()
  @IsEnum(SortKind)
  @ApiPropertyOptional({
    example: 'asc',
    enum: SortKind,
    description: 'Sort "up" or "down"',
  })
  sort_kind?: Prisma.SortOrder;
}
