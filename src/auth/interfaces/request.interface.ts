export interface IRequest {
  user: IRequestUser;
}
export interface IRequestUser {
  id: string;
  token_id: string;
}
