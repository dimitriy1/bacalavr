export interface IPayload {
  sub: string;
  jti: string;
  exp?: number;
}
