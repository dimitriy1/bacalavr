import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { AuthService } from '../auth.service';
import { plainToInstance } from 'class-transformer';
import { LoginDto } from '../dto/login.dto';
import { validateSync } from 'class-validator';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    });
  }

  //verified callback
  async validate(email: string, password: string): Promise<any> {
    const loginDto = plainToInstance(LoginDto, { email, password });
    const validationErrors = validateSync(loginDto);
    if (validationErrors.length !== 0) {
      throw new UnprocessableEntityException({
        errors: validationErrors,
      });
    }
    const user = await this.authService.validateUser(email, password);
    if (!user) {
      throw new UnauthorizedException('Incorrect email or password');
    }
    return user;
  }
}
