import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOperation,
  ApiTags,
  ApiUnprocessableEntityResponse,
  ApiOkResponse,
  ApiBearerAuth,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import {
  ForbiddenResponse,
  UnauthorizedResponse,
  UnprocessableEntityResponse,
} from '../helpers/docs/errorSchema';
import { SuccessResponseDocs } from '../helpers/docs/successShema';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { LoginEntity, TokensEntity } from './entities/login.entity';
import { LocalAuthGuard } from './guards/local-auth-guard';
import { IRequest } from './interfaces/request.interface';
import { RefreshDto } from './dto/refresh.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { RegisterEntity } from './entities/register';
import { RegisterDto } from './dto/register.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  // #region Swagger Decorators
  @ApiUnprocessableEntityResponse(
    UnprocessableEntityResponse('first_name', 'First name is required'),
  )
  @ApiCreatedResponse({ type: RegisterEntity })
  @ApiOperation({ summary: 'Register' })
  // #endregion
  @Post('register')
  register(@Body() registerDto: RegisterDto): Promise<LoginEntity> {
    return this.authService.register(registerDto);
  }

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Login' })
  @ApiUnprocessableEntityResponse(
    UnprocessableEntityResponse('password', 'Password is required'),
  )
  @ApiForbiddenResponse(ForbiddenResponse('Incorrect credentials'))
  @ApiCreatedResponse({ type: LoginEntity })
  @ApiBody({ type: LoginDto })
  @HttpCode(200)
  // #endregion
  @UseGuards(LocalAuthGuard)
  @Post('login')
  login(@Req() { user }: IRequest) {
    return this.authService.login(user.id);
  }

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Refresh token' })
  @ApiCreatedResponse({ type: TokensEntity })
  @ApiUnauthorizedResponse(UnauthorizedResponse)
  @ApiUnprocessableEntityResponse(
    UnprocessableEntityResponse('token', 'Token is required'),
  )
  @ApiBody({ type: RefreshDto })
  @HttpCode(200)
  // #endregion
  @Post('refresh')
  refresh(@Body() refreshDto: RefreshDto): Promise<TokensEntity> {
    return this.authService.validateRefreshToken(refreshDto.token);
  }

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Logout' })
  @ApiOkResponse(SuccessResponseDocs)
  @ApiUnauthorizedResponse(UnauthorizedResponse)
  @ApiBearerAuth()
  // #endregion
  @UseGuards(JwtAuthGuard)
  @Get('/logout')
  logout(@Req() { user }: IRequest) {
    return this.authService.logout(user.token_id);
  }
}
