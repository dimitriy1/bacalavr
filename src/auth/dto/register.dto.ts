import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsDefined, IsEmail, IsString, Length } from 'class-validator';
import { EntityExists } from '../../helpers/validation/entity-exists';
export class RegisterDto {
  @Transform(({ value }) =>
    typeof value === 'string' ? value.toLowerCase() : value,
  )
  @IsDefined({ message: 'Email is required.' })
  @IsEmail({}, { message: 'Please enter a valid email address.' })
  @Length(7, 100, {
    message: 'Email must be between 7 and 150 characters long.',
  })
  @EntityExists('user', 'email', false)
  @ApiProperty({ example: 'adam@gmail.com' })
  email: string;
  @IsDefined({ message: 'Password is required.' })
  @Length(8, 32, {
    message: 'Password must be between 8 and 32 characters long.',
  })
  @IsString({ message: 'Password must be a string.' })
  @ApiProperty({ example: 'MySr0ngP@ssw0rd' })
  password: string;
  @IsDefined({ message: 'Nickname is required.' })
  @IsString({ message: 'Nickname must be a string.' })
  @Length(2, 50, {
    message: 'Nickname must be between 2 and 50 characters long.',
  })
  @ApiProperty({ example: 'Adam' })
  nickname: string;
}
