import {
  Injectable,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { IPayload } from './interfaces/payload.interface';
import { ConfigService } from '@nestjs/config';
import * as moment from 'moment';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from '../prisma/prisma.service';
import { PrismaPromise } from '@prisma/client';
import { RegisterDto } from './dto/register.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  async register(data: RegisterDto) {
    data.password = this.hashPassword(data.password);
    const user = await this.prisma.user.create({
      data,
      select: {
        id: true,
      },
    });
    return await this.login(user.id);
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.prisma.user.findUnique({
      where: { email },
      select: {
        id: true,
        email: true,
        password: true,
      },
    });
    if (user && bcrypt.compareSync(password, user.password)) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return {
        ...result,
      };
    }
    return null;
  }

  async validateRefreshToken(token: string) {
    let payload: IPayload;
    try {
      payload = this.jwtService.verify(token, {
        secret: this.configService.get('JWT_REFRESH_SECRET_KEY'),
      });
    } catch {
      throw new UnprocessableEntityException('Uncorrected token');
    }
    const refresh_token = await this.prisma.userRefreshToken.findUnique({
      where: { id: payload.jti },
      select: {
        id: true,
        access_token: { select: { user_id: true } },
        expires_at: true,
      },
    });
    if (!refresh_token || refresh_token.access_token.user_id !== payload.sub) {
      throw new UnauthorizedException('Invalid refresh token');
    }
    if (refresh_token.expires_at.valueOf() < Date.now().valueOf()) {
      await this.prisma.userRefreshToken.delete({
        where: { id: payload.jti },
      });
      throw new UnauthorizedException('Refresh token is expired');
    }
    return this.getNewTokens(payload.sub, payload.jti);
  }

  async validateAccessToken(payload: IPayload) {
    const { user_id, ...access_token } =
      await this.prisma.userAccessToken.findUniqueOrThrow({
        where: { id: payload.jti },
        select: {
          user_id: true,
          id: true,
          expires_at: true,
        },
      });
    if (access_token.expires_at.valueOf() < Date.now().valueOf()) {
      throw new UnauthorizedException('Access token is expired');
    }
    if (user_id !== payload.sub) {
      throw new UnauthorizedException('Invalid access token');
    }
    return {
      id: payload.sub,
      token_id: payload.jti,
    };
  }

  async login(user_id: string) {
    const user = await this.prisma.user.findFirstOrThrow({
      where: { id: user_id },
      select: {
        id: true,
        email: true,
        nickname: true,
      },
    });
    const tokens = await this.getNewTokens(user_id);
    return {
      user,
      ...tokens,
    };
  }

  async logout(token_id: string) {
    await this.prisma.userAccessToken.delete({
      where: { id: token_id },
    });
    return { status: 'success' };
  }

  async getNewTokens(user_id: string, access_id?: string) {
    const promiseArray: PrismaPromise<any>[] = [];
    const refresh_expires_in = +this.configService.get<string>(
      'JWT_REFRESH_EXPIRATION_TIME',
    );
    const access_expires_in = +this.configService.get<string>(
      'JWT_EXPIRATION_TIME',
    );
    promiseArray.push(
      this.prisma.userAccessToken.create({
        data: {
          user_id,
          expires_at: moment().add(refresh_expires_in, 'ms').toDate(),
          refresh_token: {
            create: {
              expires_at: moment().add(access_expires_in, 'ms').toDate(),
            },
          },
        },
        select: { id: true, refresh_token: { select: { id: true } } },
      }),
    );
    if (access_id) {
      promiseArray.push(
        this.prisma.userAccessToken.delete({
          where: { id: access_id },
        }),
      );
    }
    const [access] = await this.prisma.$transaction(promiseArray);
    const payloadAccess: IPayload = {
      sub: user_id,
      jti: access.id,
    };
    const payloadRefresh: IPayload = {
      sub: user_id,
      jti: access.refresh_token.id,
    };
    const refresh_token = this.jwtService.sign(payloadRefresh, {
      expiresIn: this.configService.get<number>('JWT_REFRESH_EXPIRATION_TIME'),
      secret: this.configService.get('JWT_REFRESH_SECRET_KEY'),
    });
    const access_token = this.jwtService.sign(payloadAccess);
    return {
      access_token,
      refresh_token,
    };
  }

  hashPassword(password: string): string {
    return bcrypt.hashSync(password, this.configService.get('JWT_SALT'));
  }
}
