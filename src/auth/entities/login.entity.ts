import { ApiProperty, OmitType } from '@nestjs/swagger';

class UserLoginEntity {
  @ApiProperty({ example: 1 })
  id: string;
  @ApiProperty({ example: 'Adam' })
  nickname: string;
  @ApiProperty({ example: 'adam@gmail.com' })
  email: string;
}

export class LoginEntity {
  @ApiProperty({ type: UserLoginEntity })
  user: UserLoginEntity;
  @ApiProperty({ example: 'ASdojahiufjhiajwmpqf0idj214877^%ASDya' })
  access_token: string;
  @ApiProperty({ example: 'ASdojahiufjhiajwmpqf0idj214877^%ASDya' })
  refresh_token: string;
}

export class TokensEntity extends OmitType(LoginEntity, ['user'] as const) {}
