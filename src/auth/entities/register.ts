import { ApiProperty, PickType } from '@nestjs/swagger';
import { RegisterDto } from '../dto/register.dto';

export class RegisterEntity extends PickType(RegisterDto, [
  'email',
  'nickname',
]) {
  @ApiProperty({ example: '12123131312' })
  id: string;
}
