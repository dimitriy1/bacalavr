import {
  Controller,
  Post,
  Body,
  Param,
  UseInterceptors,
  Req,
  UploadedFiles,
  UseGuards,
  Get,
  Query,
  ParseFilePipe,
  MaxFileSizeValidator,
} from '@nestjs/common';
import { MessagesService } from './messages.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { SuccessResponseDocs } from '../helpers/docs/successShema';
import { FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ChatIdDto } from './dto/chat-id.dto';
import { IRequest } from '../auth/interfaces/request.interface';
import { CreateMessageDto } from './dto/create-message.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { GetMessagesEntity } from './entities/get-messages.entities';
import { PaginationQueryDto } from '../helpers/dto/pagination-query.dto';
import { randomBytes } from 'crypto';

@ApiTags('Messages')
@Controller('messages')
export class MessagesController {
  constructor(private readonly messagesService: MessagesService) {}

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Create message' })
  @ApiBody({ type: CreateMessageDto })
  @ApiCreatedResponse(SuccessResponseDocs)
  @ApiConsumes('multipart/form-data')
  @ApiBearerAuth()
  // #endregion
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FilesInterceptor('files', undefined, {
      storage: diskStorage({
        destination: './storage/temp',
        filename: (req, file, cb) => {
          cb(
            null,
            `${randomBytes(30).toString('hex')}${extname(file.originalname)}`,
          );
        },
      }),
    }),
  )
  @Post(':chat_id')
  createMessage(
    @Param() { chat_id }: ChatIdDto,
    @Req() { user }: IRequest,
    @Body() createMessageDto: CreateMessageDto,
    @UploadedFiles(
      new ParseFilePipe({
        fileIsRequired: false,
        validators: [new MaxFileSizeValidator({ maxSize: 10000000 })],
      }),
    )
    files: Array<Express.Multer.File>,
  ) {
    return this.messagesService.sendMessage(
      user.id,
      chat_id,
      createMessageDto,
      files,
    );
  }

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Get messages from chat' })
  @ApiOkResponse({ type: GetMessagesEntity })
  @ApiBearerAuth()
  // #endregion
  @UseGuards(JwtAuthGuard)
  @Get(':chat_id')
  getMessages(
    @Param() { chat_id }: ChatIdDto,
    @Req() { user }: IRequest,
    @Query() dto: PaginationQueryDto,
  ): Promise<GetMessagesEntity> {
    return this.messagesService.getMessages(user.id, chat_id, dto);
  }
}
