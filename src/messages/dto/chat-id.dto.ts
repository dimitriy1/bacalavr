import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsMongoId, IsString } from 'class-validator';
import { EntityExists } from 'src/helpers/validation/entity-exists';

export class ChatIdDto {
  @EntityExists('chat', 'id')
  @IsMongoId()
  @IsString()
  @IsDefined()
  @ApiProperty({ example: '123456789021' })
  chat_id: string;
}
