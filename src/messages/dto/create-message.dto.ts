import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString, MinLength } from 'class-validator';

export class CreateMessageDto {
  @MinLength(1)
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Hello' })
  text?: string;
  @IsOptional()
  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  files?;
}
