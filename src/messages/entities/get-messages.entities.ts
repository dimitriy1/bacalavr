import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class MessageEntity {
  @ApiProperty({ example: '6461d86cbb811b6a769b1ba8' })
  id: string;
  @ApiPropertyOptional({ example: 'text' })
  text?: string;
  @ApiPropertyOptional({ example: ['path1', 'path2'] })
  files?: string[];
  @ApiProperty({ example: '2023-05-15T07:12:26.887Z' })
  created_at: Date;
  @ApiProperty({ example: 1 })
  user_id: string;
  @ApiPropertyOptional({ example: true })
  sent?: boolean;
  @ApiPropertyOptional({ example: true })
  delivered?: boolean;
}

export class GetMessagesEntity {
  @ApiProperty({ example: 1 })
  user_id: string;
  @ApiProperty({ type: MessageEntity, isArray: true })
  messages: MessageEntity[];
}
