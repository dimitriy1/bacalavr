import * as jwt from 'jsonwebtoken';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { IPayload } from '../auth/interfaces/payload.interface';

@WebSocketGateway({
  namespace: 'messages',
  cors: {
    origin: 'http://localhost:8080',
    methods: ['GET', 'POST'],
  },
})
export class ChatGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server: Omit<Server, 'adapter'> & {
    adapter: {
      rooms: Map<string, Set<string>>;
      sids: Map<string, Set<string>>;
    };
  };

  //send message from sender to other user room
  sendMessage(id: string, data) {
    this.server.to(id).emit('msgToClient', data);
  }

  //server starter logic
  afterInit(server: Server) {
    console.log('Server with sockets started');
  }

  //socket disconnect
  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }

  //socket connect
  async handleConnection(client: Socket, ...args: any[]) {
    let user: IPayload;
    try {
      user = jwt.verify(
        client.handshake.headers?.authorization.split(' ')[1],
        process.env.JWT_SECRET_KEY,
      ) as IPayload;
      if (!user || typeof user?.sub !== 'string') {
        console.log('Invalid token');
        client.emit('error', { message: 'Invalid token' });
        client.disconnect();
      }
    } catch (error) {
      console.log(error);
      client.emit('error', { message: 'Invalid token' });
      client.disconnect();
      return;
    }
    await client.join(user.sub);
    //fields for info
    const room_id = [...client.rooms][1];
    console.log(`Client ${user.sub} connected to room: ${room_id}`);
  }
}
