import {
  HttpException,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { FileHelper } from '../helpers/file-system/file-helper';
import { Prisma } from '@prisma/client';
import { PaginationQueryDto } from '../helpers/dto/pagination-query.dto';
import { ChatGateway } from 'src/messages/chat.gateway';

@Injectable()
export class MessagesService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly fileHelper: FileHelper,
    private readonly chatGateway: ChatGateway,
  ) {}

  async saveFiles(files: Array<{ path: string }>, chat_id: string) {
    try {
      return await Promise.all(
        files.map(
          async (file) => await this.fileHelper.moveTemp(file.path, chat_id),
        ),
      );
    } catch {
      throw new HttpException('Files can`t be saved', 500);
    }
  }

  async updateChatUpdatedAt(chat_id: string) {
    await this.prisma.chat.update({
      where: { id: chat_id },
      data: { updated_at: new Date() },
    });
  }

  async sendMessage(
    user_id: string,
    chat_id: string,
    dto: CreateMessageDto,
    files: Array<Express.Multer.File>,
  ) {
    if (!files?.length && !dto.text) {
      throw new UnprocessableEntityException('Message is empty');
    }
    const filesArray: string[] = [];
    if (files?.length) {
      filesArray.push(...(await this.saveFiles(files, chat_id)));
    }
    const userChats = await this.prisma.userChat.findMany({
      where: { chat_id, user_id: { not: user_id } },
      select: { user_id: true },
    });
    const data: Prisma.MessageUncheckedCreateInput = {
      user_id,
      chat_id,
      text: dto.text,
      files: filesArray,
      user_messages: {
        createMany: {
          data: userChats.map(({ user_id }) => ({
            chat_id,
            user_id,
          })),
        },
      },
    };
    const messageSended = await this.prisma.message.create({
      data,
      select: {
        user_messages: { select: { user_id: true } },
        chat_id: true,
        created_at: true,
        files: true,
        text: true,
      },
    });
    if (messageSended) {
      const { user_messages, ...result } = messageSended;
      await this.updateChatUpdatedAt(chat_id);
      this.chatGateway.sendMessage(
        messageSended.user_messages[0].user_id,
        result,
      );
    } else {
      if (filesArray?.length) {
        Promise.all(
          filesArray.map(async (file) => await this.fileHelper.remove(file)),
        );
      }
    }
    return {
      status: 'success',
    };
  }

  async getMessages(
    user_id: string,
    chat_id: string,
    { skip = 0, take = 100 }: PaginationQueryDto,
  ) {
    //create orderBy
    const orderBy: Prisma.MessageOrderByWithRelationInput = {
      created_at: 'desc',
    };
    //create where
    const where: Prisma.MessageWhereInput = {
      chat_id,
    };
    //create select
    const select = {
      id: true,
      text: true,
      files: true,
      created_at: true,
      user_id: true,
      user_messages: {
        select: { read: true, user_id: true },
        where: { user_id: { not: user_id } },
      },
    } satisfies Prisma.MessageSelect;
    //get messages
    const messages = await this.prisma.message.findMany({
      where,
      orderBy,
      select,
      skip,
      take,
    });
    const result = {
      user_id,
      messages: messages.map(
        ({ user_messages, user_id: user_id_m, files, ...message }) => ({
          user_id: user_id_m,
          ...message,
          files: files.map((file) => this.fileHelper.getFullPath(file)),
          sent:
            user_id === user_id_m && !user_messages[0].read ? true : undefined,
          delivered:
            user_id === user_id_m && user_messages[0].read ? true : undefined,
        }),
      ),
    };
    console.log(result.messages[0]);
    await this.prisma.userMessage.updateMany({
      where: {
        message_id: { in: messages.map(({ id }) => id) },
        user_id,
      },
      data: { read: true },
    });
    return result;
  }
}
