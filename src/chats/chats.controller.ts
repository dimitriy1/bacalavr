import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  Query,
} from '@nestjs/common';
import { ChatsService } from './chats.service';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOperation,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateChatDto } from './dto/create-chat.dto';
import { SuccessResponseWithIdDocs } from '../helpers/docs/successShema';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { IRequest } from '../auth/interfaces/request.interface';
import { GetChatsQueryDto } from './dto/get-chats-query.dto';
import { SearchEntity } from './entities/get-chats.entities';

@ApiTags('Chats')
@Controller('chats')
export class ChatsController {
  constructor(private readonly chatsService: ChatsService) {}

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Create one chat between people' })
  @ApiBody({ type: CreateChatDto })
  @ApiCreatedResponse(SuccessResponseWithIdDocs)
  // #endregion
  @UseGuards(JwtAuthGuard)
  @Post()
  createChat(
    @Body() createChatDto: CreateChatDto,
    @Req() { user: { id } }: IRequest,
  ) {
    return this.chatsService.createChat(id, createChatDto);
  }

  // #region Swagger Decorators
  @ApiOperation({ summary: 'Get chats' })
  @ApiOkResponse({ type: SearchEntity })
  // #endregion
  @UseGuards(JwtAuthGuard)
  @Get()
  getChats(
    @Req() { user: { id } }: IRequest,
    @Query() getChatsQueryDto: GetChatsQueryDto,
  ): Promise<SearchEntity[]> {
    return this.chatsService.getChats(id, getChatsQueryDto);
  }
}
