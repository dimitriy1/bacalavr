import { Module } from '@nestjs/common';
import { ChatsService } from './chats.service';
import { ChatsController } from './chats.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { FileHelper } from 'src/helpers/file-system/file-helper';

@Module({
  imports: [PrismaModule],
  controllers: [ChatsController],
  providers: [ChatsService, FileHelper],
})
export class ChatsModule {}
