import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString, MinLength } from 'class-validator';

import { PaginationQueryDto } from '../../helpers/dto/pagination-query.dto';

export class GetChatsQueryDto extends PaginationQueryDto {
  @MinLength(1)
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    example: 'Lo',
    description: 'String for universal search',
  })
  universal?: string;
}
