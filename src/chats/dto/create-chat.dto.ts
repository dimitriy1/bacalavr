import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsMongoId } from 'class-validator';

import { EntityExists } from '../../helpers/validation/entity-exists';

export class CreateChatDto {
  @EntityExists('user')
  @IsMongoId()
  @IsDefined({ message: 'id must be filled' })
  @ApiProperty({ example: '35353535345' })
  id: string;
}
