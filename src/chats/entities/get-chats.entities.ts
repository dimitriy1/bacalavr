import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class MessageEntity {
  @ApiPropertyOptional({ example: 'text' })
  text?: string;
  @ApiPropertyOptional({ example: ['path1', 'path2'] })
  files?: string;
  @ApiProperty({ example: '2023-05-15T07:12:26.887Z' })
  created_at: Date;
  @ApiProperty({ example: 4 })
  unread_count: number;
  @ApiPropertyOptional({ example: true })
  sent?: boolean;
  @ApiPropertyOptional({ example: true })
  delivered?: boolean;
}

class UserEntity {
  @ApiProperty({ example: 'email' })
  email: string;
  @ApiProperty({ example: 'Name' })
  nickname: string;
}

export class SearchEntity {
  @ApiProperty({ example: '6461d86cbb811b6a769b1ba8' })
  id: string;
  @ApiProperty({ type: UserEntity })
  user: UserEntity;
  @ApiPropertyOptional({ type: MessageEntity })
  messages?: MessageEntity;
}
