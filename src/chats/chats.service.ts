import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { Prisma } from '@prisma/client';
import { GetChatsQueryDto } from './dto/get-chats-query.dto';
import { FileHelper } from 'src/helpers/file-system/file-helper';

@Injectable()
export class ChatsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly fileHelper: FileHelper,
  ) {}

  async createChat(user_id: string, { id }: CreateChatDto) {
    const ids = [user_id, id];
    //chat was created?
    const createdChat = await this.prisma.chat.findFirst({
      where: {
        user_chats: { every: { user_id: { in: ids } } },
      },
      select: {
        id: true,
      },
    });
    //return chat_id if chat was created
    if (createdChat) {
      return { status: 'success', id: createdChat.id };
    }
    //create chat
    const chat = await this.prisma.chat.create({
      data: {
        user_chats: {
          createMany: {
            data: ids.map((id) => ({
              user_id: id,
            })),
          },
        },
      },
      select: { id: true },
    });
    return { status: 'success', id: chat.id };
  }

  async getChats(
    user_id: string,
    { skip = 0, take = 10, universal }: GetChatsQueryDto,
  ) {
    //create where
    const where: Prisma.ChatWhereInput = {
      user_chats: { some: { user_id } },
    };
    if (universal) {
      where.OR = [
        {
          user_chats: { some: { user: { nickname: { contains: universal } } } },
        },
        {
          messages: {
            some: { text: { contains: universal } },
          },
        },
      ];
    }
    //get chats
    const chats = await this.prisma.chat.findMany({
      where,
      select: {
        id: true,
        user_chats: {
          where: { user_id: { not: user_id } },
          select: {
            user: {
              select: {
                nickname: true,
                email: true,
              },
            },
          },
        },
        user_messages: {
          where: { user_id, read: false },
          select: { id: true },
        },
        messages: {
          select: {
            user_id: true,
            text: true,
            files: true,
            created_at: true,
            user_messages: {
              orderBy: { created_at: 'desc' },
              select: { read: true, user_id: true },
              where: { user_id: { not: user_id } },
              take: 1,
            },
          },
          take: 1,
          orderBy: { created_at: 'desc' },
        },
      },
      orderBy: { created_at: 'desc' },
      skip,
      take,
    });
    return chats.map(({ messages, id, user_messages, user_chats }) => ({
      id,
      user: user_chats[0].user,
      messages:
        messages.length > 0
          ? {
              created_at: messages[0].created_at,
              text: messages[0].text,
              files: this.fileHelper.getFullPath(messages[0]?.files[0]),
              unread_count: user_messages.length,
              sent:
                user_id === messages[0].user_id &&
                messages[0].user_messages[0].read === false
                  ? true
                  : undefined,
              delivered:
                user_id === messages[0].user_id &&
                messages[0].user_messages[0].read === true
                  ? true
                  : undefined,
            }
          : null,
    }));
  }
}
